" Copyright (c) 2015 Derek Peruo
" Licensed under the MIT License
"
" http://github.com/dperuo


set nocompatible                " disable support for vi
                                " must be first because it changes other options

set modelines=0                 " prevent security exploit using mode lines

set autoread                    " reload files changed outside vim
set clipboard=unnamed           " use the system clipboard
set encoding=utf-8              " set encoding to UTF-8
set laststatus=2                " always show status line
set number                      " enable line numbers
set relativenumber              " set relative line numbers
set ruler                       " show curser position in status line
set wrap lbr                    " set word wrapping and line breaks by default

set ignorecase                  " ignore case when search pattern is all lowercase
set smartcase                   "

set incsearch                   " highlight search results as you type
set showmatch                   "
set hlsearch                    "

set gdefault                    " apply substitutions globally by default

filetype on                     " detect fileypes
syntax enable                   " enable syntax highlighting
set showmatch                   " show matching parenthesis

set tabstop=2                   " enable softtabs, 2 spaces
set shiftwidth=2                "
set softtabstop=2               "
set expandtab                   "

set autoindent                  " enable auto indentation
set copyindent                  " copy previous indentation when auto-indenting

set visualbell                  " flash screen instead of beep
set ttyfast                     " enable fast scrolling

if exists('+autochdir')
  set autochdir                 " set working director to location of open file
endif

" enable backspacing over autoindent, line breaks, and start of insert
set backspace=indent,eol,start

" highlight status line as green when in insert mode
if version >= 700
  au InsertEnter * hi StatusLine ctermfg=46 ctermbg=16
  au InsertLeave * hi StatusLine ctermfg=231 ctermbg=16
endif





" remap keys "
""""""""""""""

" disable arrow keys in all modes
noremap <Up> <nop>
noremap <Down> <nop>
noremap <Left> <nop>
noremap <Right> <nop>

" automatic pairings
" inoremap ( ()<LEFT>
" inoremap < <><LEFT>
" inoremap [ []<LEFT>
" inoremap { {}<LEFT>
" inoremap " ""<LEFT>
" inoremap ' ''<LEFT>

" remap keys
nnoremap ; :
inoremap jj <ESC>

" leader shortcuts
let mapleader = ","

nnoremap <leader>e  :set wrap!<CR>
nnoremap <leader>ee :set wrap! lbr!<CR>
nnoremap <leader>f  :set syntax=fountain<CR>
nnoremap <leader>g  :%le<CR>:g/./normal vipJgg<CR>:noh<CR>
nnoremap <leader>h  :noh<CR>
nnoremap <leader>l  :set rnu!<CR>
nnoremap <leader>m  :set syntax=markdown<CR>
nnoremap <leader>n  :set syntax=none<CR>
nnoremap <leader>c  :set syntax=scss<CR>
nnoremap <leader>r  :sort<CR>
nnoremap <leader>s  :set spell!<CR>
nnoremap <leader>t  :set list! lcs=tab:»·,trail:•,nbsp:¬<CR>
nnoremap <leader>v  :spl $MYVIMRC<CR>

" move curser as expected regardless of word wrap
nmap $ g$
nmap 0 g0
nmap ^ g^
nmap j gj
nmap k gk
vmap $ g$
vmap 0 g0
vmap ^ g^
vmap j gj
vmap k gk





" auto commands "
"""""""""""""""""

" syntax highlighting
if has("autocmd")
    au BufRead,BufNewFile *.fountain,*.spmd    setf fountain
    au BufRead,BufNewFile *.markdown,*.mdown,*.mkd,*.mkdn,*.mdwn,*.md    setf markdown
    au BufRead,BufNewFile *.less  setf less
endif

" Source the vimrc file after saving it
if has("autocmd")
    au BufWritePost .vimrc source $MYVIMRC
endif
