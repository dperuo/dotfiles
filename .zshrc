# Copyright (c) 2015 Derek Peruo
# Licensed under the MIT License
#
# http://github.com/dperuo
#


# set zsh as default shell using
# chsh -s /bin/zsh


# Load modules
source ~/.zshrc-paths
source ~/.zshrc-aliases
source ~/.zshrc-quick-launch
source ~/.zshrc-utils


# Exports
export EDITOR=$editor
export PATH=$custom_path:$PATH


# Custom Left Prompt: [directory][space][❯][space]
PROMPT="%c ❯ "

# Conditional Right Prompt: [bracket][exit code][bracket] if > 0
RPROMPT="%(?..[%?])"


# Autocomplete everything
zmodload zsh/complist
autoload -U compinit && compinit

# Format all autocomplete messages as bold, prefixed with ----
zstyle ':completion:*' format '%B---- %d%b'
# Group autocomplete options by tag name
zstyle ':completion:*' group-name ''
# Activate autocomplete selection using <tab> key
zstyle ':completion:*' menu select

# Autocomplete known remote hosts
zstyle ':completion:*' hosts $hosts
